# GrāfDB

GrāfDB is a hypergraph store 

A GrāfDB instance has two responsibilities: provide storage for content-addressed node data, and keep track of zero or more hypergraphs.

A hypergraph consists of zero or more hyperedges and one or more nodes. 

A hyperedge connects one or more nodes, each node is connected via a hyperlink.

The set of nodes is unbouded, but only those nodes connected to a hyperedge are considered part of a hypergraph.

A node may be a hyperedge, supporting nesting or embedding.

Q: How is versioning, play forward/ play back handled for neted hyperedges.
A: Maybe hyperedges are embedded via commit hash (immutable), hypergraphs are embedded via named entry (mutable) but how do we register the snapshot version or lockstep embedded graph with parent graph?

GrāfDB can be implemented on top of any underlying storage system that provides key/value storage with at least optional optimistic concurrency. We only use optimistic concurrency to store the current value of each dataset. Chunks themselves are immutable.



