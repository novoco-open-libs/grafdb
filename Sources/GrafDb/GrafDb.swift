import Foundation
import Flynn

class GrafDb: Actor {
    
    lazy var bePrint = Behavior(self) { (args: BehaviorArgs) in
        // flynnlint:parameter String - string to print
        print(args[x:0])
      }
    
    lazy var beOpenGraf = Behavior(self) { (args: BehaviorArgs) in
        // flynnlint:parameter String - string to print
        print("Opening existing Graf:", args[x:0])
      }
    
    lazy var beGetGraf = Behavior(self) { (args: BehaviorArgs) in
        // flynnlint:parameter String - string to print
        print("Opening existing Graf, or create new Graf if missing:", args[x:0])
      }
}
