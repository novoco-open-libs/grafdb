import Foundation
import Flynn

class Cell: Actor {
    
    lazy var bePrint = Behavior(self) { (args: BehaviorArgs) in
        // flynnlint:parameter String - string to print
        print(args[x:0])
      }
}
