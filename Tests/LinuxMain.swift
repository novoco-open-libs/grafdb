import XCTest

import GrafDbTests

var tests = [XCTestCaseEntry]()
tests += GrafDbTests.allTests()
XCTMain(tests)
