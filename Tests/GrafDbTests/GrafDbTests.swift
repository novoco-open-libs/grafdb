import Foundation
import Quick
import Nimble
import HashDb
import GrafDb

final class GrafDbTests: QuickSpec {
    override func spec() {
        
        func doTestInitialize() {
            it("can't initialize hashdb") {
                expect(true).to(equal(true))
            }
        }
        
        func doTestAddRemoveTriples(_ values: [(s: String, p: String, o: String)]) {
            it("addremovetriple") {
        
                expect(true).to(equal(true))
            }
        }
        
        describe("initialization") {
            doTestInitialize()
        }
        
        describe("add/remove") {
            doTestAddRemoveTriples([("","","")])
        }
    }
}
